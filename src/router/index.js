import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Photo from "../views/Photo.vue";
import Video from "../views/Video.vue";
import Finish from "../views/Finish.vue";


Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    redirect: '/photo',
    children: [
      {
        path: "/video",
        name: "Video",
        component: Video,
      },
      {
        path: "/photo",
        name: "Photo",
        component: Photo,
      },
      {
        path: "/finish",
        name: "Finish",
        component: Finish,
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
